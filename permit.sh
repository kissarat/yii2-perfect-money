#!/bin/bash

chmod a+w runtime
chmod a+w web/assets
chmod a+w models
chmod a+w models/search
chmod a+w controllers
chmod a+w views
