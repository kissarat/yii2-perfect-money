<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/23/15
 * Time: 2:23 AM
 */

namespace app\controllers;


use app\models\User;
use app\models\UserData;
use Yii;
use yii\web\Controller;

class HomeController extends Controller {
    public function actionIndex() {
        $social = Yii::$app->session->get('social');
        if ($social) {
            Yii::$app->session->remove('social');
            if (Yii::$app->user->isGuest && UserData::count() > 0) {
                return $this->redirect(['user/signup']);
            }
            else {
                return $this->redirect(['user/view', 'name' => Yii::$app->user->identity->name]);
            }
        }
        return $this->render('index');
    }

    public function actionError() {
        $exception = Yii::$app->getErrorHandler()->exception;
        $message = $exception->getMessage();
        if ($message) {
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->render('error', [
            'exception' => $exception
        ]);
    }
}
