<?php

namespace app\controllers;

use app\behaviors\Access;
use app\behaviors\NoTokenValidation;
use Yii;
use app\models\Invoice;
use app\models\search\Invoice as InvoiceSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class InvoiceController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'success' => ['post'],
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::className(),
                'plain' => ['success', 'fail', 'index', 'view', 'create'],
                'admin' => ['update', 'delete']
            ],

            'no_csrf' => [
                'class' => NoTokenValidation::className(),
                'only' => ['success', 'fail'],
            ]
        ];
    }

    public function actionIndex($user = null) {
        $searchModel = new InvoiceSearch();
        if (!Yii::$app->user->identity->isManager()) {
            if (!$user) {
                return $this->redirect(['index', 'user' => Yii::$app->user->identity->name]);
            }
            elseif (!Yii::$app->user->identity->isManager() && $user != Yii::$app->user->identity->name) {
                throw new ForbiddenHttpException('Forbidden');
            }
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Invoice([
            'user_name' => Yii::$app->user->identity->name,
            'status' => 'create'
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSuccess($id) {
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $this->findModel($id);
//        $invoice->scenario = 'payment';
        $string = $string= $_POST['PAYMENT_ID']
            .':'.$_POST['PAYEE_ACCOUNT']
            .':'.$_POST['PAYMENT_AMOUNT']
            .':'.$_POST['PAYMENT_UNITS']
            .':'.$_POST['PAYMENT_BATCH_NUM']
            .':'.$_POST['PAYER_ACCOUNT']
            .':'. Yii::$app->perfect->hashAlternateSecret()
            .':'.$_POST['TIMESTAMPGMT'];
        $user = $invoice->user;
        if (Yii::$app->user->identity->name != $invoice->user_name) {
            $transaction->rollBack();
            throw new ForbiddenHttpException('Вы можете изменять статус только своих оплат');
        }
        elseif ('success' == $invoice->status) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('success', "Оплата #$id уже осуществлена");
        }
        elseif (strtoupper(md5($string)) != $_POST['V2_HASH']) {
            $invoice->saveStatus('invalid_hash');
            Yii::$app->session->setFlash('error', Invoice::$statuses['invalid_hash']);
        }
        elseif ($invoice->amount != $_POST['PAYMENT_AMOUNT']) {
            $invoice->saveStatus('invalid_amount');
            Yii::$app->session->setFlash('error', Invoice::$statuses['invalid_amount']);
        }
        elseif (Yii::$app->perfect->wallet != $_POST['PAYEE_ACCOUNT']) {
            $invoice->saveStatus('invalid_receiver');
            Yii::$app->session->setFlash('error', "Неправильный получатель " . $_POST['PAYEE_ACCOUNT']);
        }
        elseif ('USD' != $_POST['PAYMENT_UNITS']) {
            $invoice->saveStatus('invalid_currency');
            Yii::$app->session->setFlash('error', Invoice::$statuses['invalid_currency']);
        }
        else {
            $user->account += $invoice->amount;
            if ($invoice->saveStatus('success') && $user->save()) {
                Yii::$app->session->setFlash('success', "Оплата #$id осуществелна");
            }
            else {
                $invoice->saveStatus('fail');
                Yii::$app->session->setFlash('error', "При сохранении оплаты #$id случилась ошибка");
            }
        }
        $transaction->commit();

        return $this->render('view', [
            'model' => $invoice
        ]);
    }

    public function actionFail($id) {
        $invoice = $this->findModel($id);

        if (Yii::$app->user->identity->name != $invoice->user_name) {
            throw new ForbiddenHttpException('Вы можете изменять статус только своих оплат');
        }
        else {
            $invoice->saveStatus('cancel');
            Yii::$app->session->setFlash('error', "Вы не осуществили оплату");
        }

        return $this->render('view', [
            'model' => $invoice
        ]);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
