<?php

namespace app\controllers;

use app\behaviors\Access;
use app\behaviors\Journal;
use app\models\Login;
use app\models\Password;
use app\models\Record;
use app\models\ResetRequest;
use app\models\Signup;
use app\models\UserData;
use Yii;
use app\models\User;
use app\models\search\User as UserSearch;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class UserController extends Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => Access::className(),
                'plain' => ['view', 'update'],
                'admin' => ['delete']
            ]
        ];
    }

    public function actions() {
        return [
            'social' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'social']
            ]
        ];
    }

    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($name) {
        $model = $this->findModel($name);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate() {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'name' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($name) {
        $model = $this->findModel($name);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->isNewRecord || (!Yii::$app->user->identity->isAdmin() && $model->isAttributeChanged('perfect', false))) {
                Yii::$app->session->setFlash('error', 'Wallet can change admin only');
            }
            else {
                return $this->redirect(['view', 'name' => $model->name]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($name) {
        $this->findModel($name)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param string $name
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($name) {
        if (($model = User::findOne(['name' => $name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function social(ClientInterface $client) {
        $user_data = new UserData($client->getName(), $client->getUserAttributes());
        if (Yii::$app->user->isGuest) {
            $user = User::findSocial($user_data->getObjectName(), $user_data->getObjectName());
            if ($user) {
                Login::login($user);
            }
            else {
                $user_data->save();
            }
        }
        else {
            User::saveSocial(Yii::$app->user->identity, $user_data);
        }
        Yii::$app->session->set('social', $client->getName());
    }

    public function actionSignup() {
        $model = new User();
        User::loadUserData($model);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->generateCode();
            $model->status = User::PLAIN;
            $model->save(false);
            if ($model->social) {
                User::saveSocial($model, $model->social);
            }
            $url = Url::to(['password', 'code' => $model->code], true);
            if ($model->sendEmail('html', [
                'subject' => Yii::$app->params['site']['name'] . ' ' . Yii::t('app', 'registration'),
                'content' => "Перейдите по <a href='$url'>ссылке</a>"
            ])) {
                Yii::$app->session->setFlash('info', Yii::t('app', 'Check your email'));
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Email send error'));
            }
            return $this->redirect(['home/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionLogin() {
        $model = new Login();

        if ($model->load(Yii::$app->request->post())) {
            $user = $model->getUser();
            if ($user) {
                $can = $user->canLogin();
                if ($can && $user->validatePassword($model->password)) {
                    if ($user->status > 0) {
                        Yii::$app->user->login($user);
                        return $this->redirect(['view', 'name' => $user->name]);
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Ваш аккаунт заблокирован'));
                    }
                }
                else {
                    Journal::write('user', 'login_fail', $user->id);
                    if ($can) {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
                    }
                    else {
                        $record = Record::find()->where([
                            'object_id' => $user->id,
                            'event' => 'login_fail'
                        ])->orderBy(['time' => SORT_DESC])->one();
                        Yii::$app->session->setFlash('error',
                            "Вы превысили максимальное количество попыток входа, вы сможете войти после $record->time");
                    }
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->redirect(['home/index']);
    }

    public function actionPassword($code = null) {
        $message = null;
        $model = null;
        $user = $code ? User::findOne(['code' => $code]) :  Yii::$app->user->identity;
        if ($user) {
            $model = new Password([
                'scenario' => $code ? 'reset' : 'default',
                'user' => $user
            ]);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($code) {
                    $user->code = null;
                    if (!$user->auth) {
                        $user->generateAuthKey();
                    }
                    $user->setPassword($model->new_password);
                    if ($user->save()) {
                        return $this->redirect(['user/login']);
                    } else {
                        $message = 'User validation error';
                    }
                } else {
                    if ($user->validatePassword($model->password)) {
                        $user->setPassword($model->new_password);
                        if ($user->save()) {
                            return $this->redirect(['user/view', 'name' => $user->name]);
                        }  else {
                            $message = 'User validation error';
                        }
                    }
                    else {
                        $model->addError('password', Yii::t('app', 'Invalid password'));
                    }
                }
            }
        }
        else {
            $message = Yii::t('app', 'Invalid code');
        }
        return $this->render('password', [
            'model' => $model,
            'message' => $message
        ]);
    }

    public function actionRequest() {
        $model = new ResetRequest();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findOne(['email' => $model->email]);
            $user->generateCode();
            if ($user->save()) {
                $url = Url::to(['password', 'code' => $user->code], true);
                if($user->sendEmail('html', [
                    'subject' => Yii::$app->params['site']['name'] . ' ' . Yii::t('app', ' password reset'),
                    'content' => "Для восстановления пароля ерейдите по <a href='$url'>ссылке</a>"
                ])) {
                    Yii::$app->session->setFlash('info', Yii::t('app', 'Check your email'));
                    return $this->redirect(['site/index']);
                }
                else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Email send error'));
                }
            }
        }
        return $this->render('request', [
            'model' => $model
        ]);
    }
}
