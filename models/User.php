<?php

namespace app\models;


use app\behaviors\Journal;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class User
 * @property integer id
 * @property string name
 * @property string auth
 * @property string email
 * @property string code
 * @property string hash
 * @property integer status
 * @property array socials
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface {

    const BLOCKED = 0;
    const ADMIN = 1;
    const PLAIN = 2;
    const MANAGER = 3;

    public $social;

    public static function statuses() {
        return [
            User::BLOCKED => Yii::t('app', 'Blocked'),
            User::ADMIN => Yii::t('app', 'Admin'),
            User::PLAIN => Yii::t('app', 'Registered'),
            User::MANAGER => Yii::t('app', 'Manager')
        ];
    }

    public function traceable() {
        return ['status', 'email', 'skype', 'phone', 'perfect', 'account', 'facebook', 'vkontakte', 'twitter'];
    }

    public function url() {
        return ['user/view', 'name' => $this->name];
    }

    public function rules() {
        return [
            ['id', 'integer'],
            [['name', 'email', 'phone', 'skype', 'perfect'], 'required'],
            ['name', 'string', 'min' => 3, 'max' => 48],
            ['name', 'match', 'pattern' => '/^[a-z][a-z0-9_\-]+$/i'],
            ['email', 'email'],
            ['perfect', 'match', 'pattern' => '/^U\d{7}$/', 'message' => 'Формат должен быть U1234567'],
            ['skype', 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$/'],
            ['phone', 'match', 'pattern' => '/^\d{9,16}$/', 'message' =>
                'Номер телефона должен состоять только из цифр и содержать код страны и региона'],

            [['name', 'email', 'skype', 'phone', 'skype', 'perfect'], 'filter', 'filter' => 'trim'],
            [['name', 'email', 'skype', 'phone', 'skype', 'perfect'], 'unique',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'This value has already been taken')],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'account' => Yii::t('app', 'Account'),
            'skype' => Yii::t('app', 'Skype'),
            'phone' => Yii::t('app', 'Phone'),
            'perfect' => Yii::t('app', 'Perfect Money wallet'),
        ];
    }

    public function behaviors() {
        return [
            Journal::className()
        ];
    }

    /**
     * @param string $id
     * @return User
     */
    public static function findIdentity($id) {
        return parent::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new Exception('Not implemented findIdentityByAccessToken');
    }

    /**
     * @param $name
     * @param $id
     * @return User
     */
    public static function findSocial($name, $id) {
        return static::findOne([$name => $id]);
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->auth;
    }

    public function validateAuthKey($authKey) {
        return $authKey == $this->auth;
    }

    public function validatePassword($password) {
        return password_verify($password, $this->hash);
    }

    public function setPassword($value) {
        $this->hash = password_hash($value, PASSWORD_DEFAULT);
    }

    public function generateAuthKey() {
        $this->auth = Yii::$app->security->generateRandomString(64);
    }

    public function generateCode() {
        $this->code = Yii::$app->security->generateRandomString(64);
    }

    public static function saveSocial(User $model, UserData $user_data) {
        $model->{$user_data->getObjectName()} = $user_data->id;
        $model->save();
        $user_data->save();
    }

    public function &getSocials() {
        $socials = [];
        foreach(['facebook', 'vkontakte', 'twitter'] as $social) {
            $socials[$social] = $this->$social;
        }
        return $socials;
    }

    public function &getSocialLinks() {
        $links = [];
        foreach($this->getSocials() as $name => $id) {
            if ($id) {
                $link = null;
                switch ($name) {
                    case 'facebook':
                        $link = 'https://facebook.com/' . $id;
                        break;
                    case 'vkontakte':
                        $link = 'https://vk.com/id' . $id;
                        break;
                }
                $links[$name] = $link;
            }
        }
        return $links;
    }

    public function isManager() {
        return static::ADMIN == $this->status || static::MANAGER == $this->status;
    }

    public function isAdmin() {
        return static::ADMIN == $this->status;
    }

    public function journal(Record $record) {
    }

    public function __toString() {
        return $this->name;
    }

    public static function loadUserData(User $signup) {
        $signup->social = UserData::load();
        if (!$signup->social) {
            return null;
        }
        switch ($signup->social->getObjectName()) {
            case 'facebook':
                $signup->email = $signup->social->email;
                break;
            case 'vkontakte':
            case 'twitter':
                $signup->name = $signup->social->screen_name;
                break;
        }
    }

    public function sendEmail($template, $params)
    {
        $template = Yii::getAlias('@app') . "/mail/$template.php";
        return Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom(['kissarat@gmail.com' => 'admin'])
            ->setSubject($params['subject'])
            ->setHtmlBody(Yii::$app->view->renderFile($template, $params))
            ->send();
    }

    public function canLogin() {
        return Record::find()->andWhere([
            'object_id' => $this->id,
            'event' => 'login_fail'
        ])
            ->andWhere('time > DATE_SUB(NOW(), INTERVAL 5 MINUTE)')
            ->count() < 10;
    }
}
