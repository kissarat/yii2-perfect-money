<?php

namespace app\models;

use PDO;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "object".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $auth
 * @property string $name
 *
 * @property array $attributes
 * @property User $userName
 */
class Object extends ActiveRecord
{
    private $_attributes;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name'], 'string', 'max' => 24],
            [['auth'], 'string', 'max' => 64],
            [['name'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'User Name'),
            'auth' => Yii::t('app', 'Auth'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes() {
        if (!$this->_attributes) {
            $this->_attributes = [];
            $command = Yii::$app->db->createCommand('SELECT name, value FROM attribute WHERE object_id = :id', [
                ':id' => $this->id
            ]);
            $command->execute();
            while($attr = $command->pdoStatement->fetch(PDO::FETCH_NUM)) {
                $value = $attr[1];
                if (strpos($value, 'a:') === 0) {
                    $value = unserialize($value);
                }
                elseif (is_numeric($value)) {
                    $value = (float) $value;
                }
                $this->_attributes[$attr[0]] = $value;
            }
        }
        return $this->_attributes;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['name' => 'user_name']);
    }
}
