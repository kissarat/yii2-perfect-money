<?php

namespace app\models;

use app\behaviors\Journal;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property string $user_name
 * @property decimal $amount
 * @property string $status
 *
 * @property User $userName
 */
class Invoice extends ActiveRecord
{

    public static $statuses = [
        'create' => 'Создано',
        'invalid_amount' => 'Оплачена несоотвествующая сумма',
        'invalid_receiver' => 'Неправильний получатель',
        'invalid_batch' => 'Несоотвествие номеров транзакций',
        'invalid_response' => 'Неизвестний ответ сервера',
        'invalid_hash' => 'Ошибка подсчета хеш-суммы',
        'invalid_currency' => 'Оплата осуществлена не в USD',
        'insufficient_funds' => 'Недостаточно средств на счету пользователя',
        'no_qualification' => 'Пользователь не прошел квалификацию',
        'cancel' => 'Отмена',
        'fail' => 'Ошибка',
        'success' => 'Осуществлено'
    ];

    public static function tableName() {
        return 'invoice';
    }

    public function traceable() {
        return ['status'];
    }

    public function behaviors() {
        return [
            Journal::className()
        ];
    }

    public function rules() {
        return [
            [['user_name', 'amount'], 'required'],
            [['amount'], 'number'],
            [['user_name', 'status'], 'string', 'max' => 16]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'User Name'),
            'amount' => Yii::t('app', 'Amount'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['name' => 'user_name']);
    }

    public function saveStatus($status) {
        $this->status = $status;
        return $this->save();
    }

    public function __toString() {
        return "$$this->amount $this->user_name #$this->id";
    }
}
