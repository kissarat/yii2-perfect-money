<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/23/15
 * Time: 12:35 PM
 */

namespace app\models;


use Exception;
use PDO;
use Yii;

/**
 * Class UserData
 * @property string objectName
 * @package app\models
 */
class UserData {
    private $_id;
    private $_object_name;
    private $_attributes;

    public function __construct($name, $attributes = []) {
        $this->_object_name = $name;
        $this->_attributes = $attributes;
    }

    /**
     * @param string $name
     * @return UserData|null
     * @throws Exception
     */
    public static function load($name = null) {
        $db = Yii::$app->db;
        if ($name) {
            $command = $db->createCommand('SELECT id FROM object WHERE name = :name AND user_name = :user_name', [
                ':name' => $name,
                ':user_name' => Yii::$app->user->identity->name
            ]);
        }
        else {
            if (empty($_COOKIE['PHPSESSID'])) {
                Yii::$app->session->open();
            }
            $command = $db->createCommand('SELECT id, `name` FROM object WHERE auth = :auth', [
                ':auth' => $_COOKIE['PHPSESSID']
            ]);
        }
        if ($command->execute() > 0) {
            $object = $command->pdoStatement->fetchObject();
            $user_data = new UserData($object->name);
            $user_data->_id = $object->id;
            $command = $db->createCommand('SELECT name, value FROM attribute WHERE object_id = :id', [
                ':id' => $user_data->_id
            ]);
            if ($command->execute() > 0) {
                while($attribute = $command->pdoStatement->fetchObject()) {
                    $user_data->_attributes[$attribute->name] = $attribute->value;
                }
            }
            return $user_data;
        }
        return null;
    }

    public static function loadByUser($user_name) {
        $command = Yii::$app->db->createCommand('SELECT type, name, value FROM object_attribute WHERE user_name = :user_name', [
            ':user_name' => $user_name
        ]);
        $command->execute();
        $attributes = [];
        while($row = $command->pdoStatement->fetch(PDO::FETCH_NUM)) {
            $attributes[$row[0]][$row[1]] = $row[2];
        }
        return $attributes;
    }

    public static function count() {
        $command = Yii::$app->db->createCommand('SELECT count(*) FROM object WHERE auth = :auth', [
            ':auth' => Yii::$app->session->id
        ]);
        $command->execute();
        return $command->pdoStatement->fetchColumn();
    }

    public function save() {
        $db = Yii::$app->db;
        $id = null;
        $attributes = [];
        foreach($this->_attributes as $name => $value) {
            if (!empty($value) || is_bool($value)) {
                if (is_array($value) || is_object($value)) {
                    $value = serialize($value);
                }
                $attributes[$name] = $value;
            }
        }
        $user_name = Yii::$app->user->isGuest ? null : Yii::$app->user->identity->name;
        if ($this->_id) {
            $id = $this->_id;
            $db->createCommand('UPDATE object SET user_name = :user_name, auth = null, name = :name WHERE id = :id', [
                ':user_name' => $user_name,
                ':name' => $this->getObjectName(),
                ':id' => $id
            ])->execute();
            foreach($attributes as $name => $value) {
                $command = $db->createCommand('SELECT count(*) FROM attribute WHERE object_id = :object_id AND `name` = :name', [
                    ':object_id' => $this->_id,
                    ':name' => $name,
                ]);
                $command->execute();
                if ($command->pdoStatement->fetchColumn() > 0) {
                    $db->createCommand('UPDATE attribute SET `value` = :value
                      WHERE object_id = :object_id AND `name` = :name AND `value` <> :value', [
                        ':object_id' => $id,
                        ':name' => $name,
                        ':value' => $value
                    ])->execute();
                    unset($attributes[$name]);
                }
            }
        }
        else {
            $db->createCommand('INSERT INTO object(user_name, auth, `name`) VALUES (:user_name, :auth, :name)', [
                ':user_name' => $user_name,
                ':auth' => Yii::$app->session->id,
                ':name' => $this->_object_name
            ])->execute();
            $id = $db->lastInsertID;
        }

//        throw new Exception(json_encode($attributes, JSON_PRETTY_PRINT));
        foreach($attributes as $key => $value) {
            $db->createCommand('INSERT INTO attribute(object_id, name, value) VALUES (:object_id, :name, :value)', [
                ':object_id' => $id,
                ':name' => $key,
                ':value' => $value
            ])->execute();
        }
    }

    public function getObjectName() {
        return $this->_object_name;
    }

    public function __get($name) {
        return $this->_attributes[$name];
    }

    public function __set($name, $value) {
        $this->_attributes[$name] = $value;
    }

    public function __toString() {
        return json_encode([
            'id' => $this->_id,
            'name' => $this->_object_name,
            'attributes' => $this->_attributes
        ]);
    }
}
