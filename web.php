<?php

use app\behaviors\Journal;

$config['components']['user'] = [
    'identityClass' => 'app\models\User',
    'enableAutoLogin' => true,
    'loginUrl' => ['user/login'],
    'on afterLogin' => function($event) {
        $event->name = 'login';
        $event->sender = Yii::$app->user->identity;
        Journal::writeEvent($event);
    },
    'on beforeLogout' => function($event) {
        $event->name = 'logout';
        $event->sender = Yii::$app->user->identity;
        Journal::writeEvent($event);
    },
];

$config['components']['errorHandler'] = [
    'errorAction' => 'home/error',
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['bootstrap'][] = 'gii';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
    $config['modules']['gii'] = 'yii\gii\Module';
}
