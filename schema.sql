CREATE TABLE `log` (
   `id`          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   `level`       INT UNSIGNED,
   `category`    TINYTEXT,
   `log_time`    DOUBLE UNSIGNED,
   `prefix`      TEXT,
   `message`     TEXT
) ENGINE ARCHIVE;


CREATE TABLE source_message (
  id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  category VARCHAR(32) DEFAULT 'app',
  message TINYTEXT
) ENGINE MyISAM;


CREATE TABLE message (
  id SMALLINT UNSIGNED,
  language VARCHAR(16) DEFAULT 'ru',
  translation TINYTEXT,
  PRIMARY KEY (id, language),
  CONSTRAINT fk_message_source_message FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE MyISAM;


CREATE VIEW ru AS
  SELECT s.id, message, translation
  FROM source_message s JOIN message t ON s.id = t.id;


DELIMITER $$
CREATE PROCEDURE translate(message TINYTEXT, translation TINYTEXT) BEGIN
  INSERT INTO source_message(`message`) VALUES (message);
  INSERT INTO message(id, `translation`) VALUES (LAST_INSERT_ID(), translation);
  END
$$
DELIMITER ;

CREATE TABLE `user` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(24) NOT NULL,
  email VARCHAR(48) NOT NULL UNIQUE,
  hash CHAR(60),
  auth CHAR(64) UNIQUE,
  code CHAR(64),
  duration INT UNSIGNED NOT NULL DEFAULT 60,
  status TINYINT NOT NULL,
  perfect CHAR(8),
  account DECIMAL(8,2) NOT NULL DEFAULT '0.00',
  phone VARCHAR(16),
  skype VARCHAR(32),
  facebook BIGINT UNSIGNED,
  vkontakte BIGINT UNSIGNED,
  twitter BIGINT UNSIGNED,
  UNIQUE INDEX (name),
  CONSTRAINT phone CHECK (LENGTH(phone) >= 9),
  CONSTRAINT skype CHECK (LENGTH(skype) >= 5)
) ENGINE InnoDB;

INSERT INTO `user`(name, email, status) VALUES ('admin', 'kissarat@gmail.com', 1);


CREATE TABLE `session` (
  id CHAR(40) NOT NULL PRIMARY KEY,
  expire INT UNSIGNED,
  data BLOB
) ENGINE MyISAM;


CREATE TABLE `cache` (
  id CHAR(128) NOT NULL PRIMARY KEY,
  expire INT UNSIGNED,
  data BLOB
) ENGINE MyISAM;


CREATE TABLE `object` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_name VARCHAR(24),
  auth CHAR(64),
  name VARCHAR(16),
  INDEX object_id(id),
  CONSTRAINT object_user FOREIGN KEY (user_name)
    REFERENCES `user`(name)
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE InnoDB;

INSERT INTO `object` VALUES (1, 'admin', null, 'email');


CREATE TABLE `attribute` (
  object_id INT UNSIGNED NOT NULL,
  name VARCHAR(16) NOT NULL,
  value BLOB NOT NULL,
  CONSTRAINT attribute_object_id FOREIGN KEY (object_id)
    REFERENCES object(id)
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE InnoDB;

INSERT INTO `attribute` VALUES (1, 'host', 'smtp.gmail.com');
INSERT INTO `attribute` VALUES (1, 'username', 'kissarat@gmail.com');
INSERT INTO `attribute` VALUES (1, 'password', 'zclmclinflhtqnjy');
INSERT INTO `attribute` VALUES (1, 'port', '465');
INSERT INTO `attribute` VALUES (1, 'encryption', 'ssl');


CREATE VIEW `object_attribute` AS
  SELECT o.id, o.name as type, user_name, a.name, value
  FROM object o JOIN attribute a ON o.id = a.object_id;


CREATE TABLE `journal` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  type VARCHAR(16) NOT NULL,
  event VARCHAR(16) NOT NULL,
  object_id INT UNSIGNED,
  data BLOB,
  user_name VARCHAR(24),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ip INT UNSIGNED,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
    REFERENCES `user`(name)
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE InnoDB;


CREATE TABLE `feedback` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(24) NOT NULL,
  email VARCHAR(48),
  subject TINYTEXT NOT NULL,
  content TEXT NOT NULL
) ENGINE InnoDB;


CREATE TABLE `invoice` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_name VARCHAR(16) NOT NULL,
  amount DECIMAL(8, 2) NOT NULL,
  batch BIGINT,
  status VARCHAR(16) DEFAULT 'create',
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
    REFERENCES `user`(name)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount <> 0)
) ENGINE InnoDB;
