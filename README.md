Yii 2 Project Template
============================

Yii 2 Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

DIRECTORY STRUCTURE
-------------------

      controllers/  contains Web controller classes
      models/       contains model classes
      vendor/       contains dependent 3rd-party packages
      views/        contains view files for the Web application
      web/          contains the entry script and Web resources
      config.php    common configuration
      web.php       web application configuration
      local.php     db configuration


REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.
