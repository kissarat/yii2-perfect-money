<?php

$config = [
    'id' => 'quiz',
    'basePath' => __DIR__,
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/index',
    'language' => 'ru',
    'charset' => 'utf-8',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\DbCache'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'page/<name:\w+>' => 'post/page',
                'signup/<authclient:\w+>' => 'user/social',
                'profile/<name:\w+>' => 'user/view',
                'profile/<name:\w+>/edit' => 'user/update',
                'journal/user/<user:[\w_\-]+>' => 'journal/index',
                'journal/<id:\d+>' => 'journal/view',
                'reset/<code:\w+>' => 'user/password',
                'feedback/template/<template:\w+>' => 'feedback/create',
                'payment/user/<user:[\w_\-]+>' => 'invoice/index',
                'payment/<id:\d+>' => 'invoice/view',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource'
                ]
            ]
        ],
        'session' => [
            'class' => 'yii\web\DbSession'
        ],
    ],
    'params' => null,
];
