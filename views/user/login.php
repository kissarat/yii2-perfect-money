<?php

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Login */
/* @var $form ActiveForm */
?>
<div class="user-login">
    <h1><?= Yii::t('app', 'Login') ?></h1>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= ''//$form->field($model, 'remember')->checkbox() ?>

    <div class="form-group">
        Вы можете <?= Html::a('восстановить пароль', ['request']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <br/>
    <?= AuthChoice::widget(['baseAuthUrl' => ['social']]) ?>

</div><!-- user-login -->
