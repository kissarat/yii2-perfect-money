<?php

use app\models\User;
use yii\authclient\widgets\AuthChoiceAsset;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

AuthChoiceAsset::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Journal'), ['journal/index', 'user' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?php
        if ($model->name == Yii::$app->user->identity->name) {
            echo Html::a(Yii::t('app', 'Change Password'), ['user/password', 'name' => $model->name], ['class' => 'btn btn-warning']);
        }
        ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'name' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?php
        if (Yii::$app->user->identity->isAdmin()) {
            echo Html::a(Yii::t('app', 'Delete'), ['delete', 'name' => $model->name
            ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email:email',
            [
                'attribute' => 'status',
                'value' => User::statuses()[$model->status]
            ],
            'account',
            'phone',
            'skype',
            'perfect'
        ],
    ]) ?>

    <?php
    foreach($model->getSocialLinks() as $social => $link) {
        echo Html::a(Html::tag('span', '', ['class' => "auth-client auth-icon $social"]),
            $link, ['class' => "auth-link $social"]);
    }
    ?>

</div>
