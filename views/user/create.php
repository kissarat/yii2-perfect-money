<?php

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::$app->user->isGuest ? Yii::t('app', 'Signup') : Yii::t('app', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (Yii::$app->user->isGuest) {
        echo '<br/>' . AuthChoice::widget(['baseAuthUrl' => ['social']]);
    }
    ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
