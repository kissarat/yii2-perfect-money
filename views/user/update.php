<?php

use yii\authclient\widgets\AuthChoiceAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

AuthChoiceAsset::register($this);

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'name' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$socials = $model->socials;
foreach($socials as $name => $id) {
    if ($id) {
        unset($socials[$name]);
    }
}
$socials = array_keys($socials);
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php
    if (Yii::$app->user->identity->name == $model->name && !empty($socials)) {
        echo Html::tag('h2', 'Привязать');
        foreach($socials as $social) {
            echo Html::a(Html::tag('span', '', ['class' => "auth-icon $social"]),
                '/signup/' . $social, ['class' => "auth-link $social"]);
        }
    }
    ?>
</div>
