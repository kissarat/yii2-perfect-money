<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

function submit($label) {
    echo '<div class="form-group">';
    echo Html::submitButton($label, ['class' => 'btn btn-success']);
    echo '</div>';
}

echo '<div class="user-form">';
$form = ActiveForm::begin();

echo $form->field($model, 'name');
echo $form->field($model, 'email');
echo $form->field($model, 'phone');
echo $form->field($model, 'skype');
if ($model->isNewRecord || Yii::$app->user->identity->isAdmin()) {
    echo $form->field($model, 'perfect');
}
else {
    $a = Html::a('обратитесь к адмнинистратору', ['feedback/create', 'template' => 'wallet']);
    echo "<div class='form-group'>Для изменения кошелька $a</div>";
}

if (Yii::$app->user->isGuest) {
    submit(Yii::t('app', 'Signup'));
}
else {
    if ($model->isNewRecord) {
        submit(Yii::t('app', 'Create'));
    }
    else {
        submit(Yii::t('app', 'Update'));
    }
}

ActiveForm::end();

echo '</div>';
