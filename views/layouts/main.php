<?php
use app\Alert;
use app\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="image" content="/image/background.jpg" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image" href="/image/background.jpg" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $items = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index']],
        ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/create']]
    ];

    if (Yii::$app->user->isGuest) {
        $items[] = ['label' => Yii::t('app', 'Login') , 'url' => ['/user/login']];
        $items[] = ['label' => Yii::t('app', 'Signup') , 'url' => ['/user/signup']];
    }
    else {
        $items[] = ['label' => Yii::t('app', 'Payment') , 'url' =>
            Yii::$app->user->identity->isManager()
                ? ['/invoice/index']
                : ['/invoice/index', 'user' => Yii::$app->user->identity->name]];
        $items[] = ['label' => Yii::t('app', 'Profile') , 'url' => ['/user/view', 'name' => Yii::$app->user->identity->name]];
        $items[] = [
            'label' => Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->name . ')',
            'url' => ['/user/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>


    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<!--footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
