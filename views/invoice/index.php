<?php

use app\models\Invoice;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;

$actions = ['class' => 'yii\grid\ActionColumn'];
if (!Yii::$app->user->identity->isManager()) {
    $actions['template'] = '{view}';
}
?>
<div class="invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Invoice'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_name',
            'amount',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return Invoice::$statuses[$model->status];
                }
            ],

            $actions,
        ],
    ]); ?>

</div>
