<?php

use app\models\Invoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin();

    if (Yii::$app->user->identity->isManager()) {
        echo $form->field($model, 'user_name')->textInput(['maxlength' => true]);
        echo $form->field($model, 'status')->dropDownList(Invoice::$statuses);
    }
    echo$form->field($model, 'amount')->textInput(['maxlength' => true]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
