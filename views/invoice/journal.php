<?php

/**
 * @var app\models\Record $record
 */

use app\models\Invoice;

$info = $record->info;
if (!$info) {
    echo '';
}
elseif (isset($info['status'])) {
    echo Invoice::$statuses[$info['status']];
}
