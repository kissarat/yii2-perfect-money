<?php
use app\models\UserData;

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../boot.php';
require __DIR__ . '/../web.php';

(new yii\web\Application($config))->run();

//Yii::$app->params = UserData::loadByUser('admin');
//foreach(Yii::$app->params['email'] as $key => $value) {
//    Yii::$app->mailer->transport->$key = $value;
//}
